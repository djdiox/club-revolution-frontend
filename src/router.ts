import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import CreatePlaylist from './views/CreatePlaylist.vue';
import ShowPlaylist from './views/ShowPlaylists.vue';
import Services from './views/Services.vue';
import Spotify from './views/Spotify.vue';
import Visual from './views/Visual.vue';
import Visual2 from './views/Visual2.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/playlist/create',
      name: 'playlistCreate',
      component: CreatePlaylist,
    },
    {
      path: '/playlist/list',
      name: 'playlistTable',
      component: ShowPlaylist,
    },
    {
      path: '/visual',
      name: 'visual',
      component: Visual,
    },
    {
      path: '/visual2',
      name: 'visual2',
      component: Visual2,
    },
    {
      path: '/services',
      name: 'services',
      component: Services,
    },
    {
      path: '/spotify',
      name: 'spotify',
      component: Spotify,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
});
