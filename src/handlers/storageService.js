export default {
  setItem(key, item) {
    return localStorage.setItem(key, item)
  },
  removeItem(key){
    return localStorage.removeItem(key)
  },
  getItem(key){
    return localStorage.getItem(key)
  },
};