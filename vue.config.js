module.exports = {
  publicPath: '/club-revolution-frontend/',
  chainWebpack: config => {
    // GraphQL Loader
    config.module
      .rule('shader')
      .test([/\.vert$/, /\.frag$/])
      .use('webpack-glsl-loader')
        .loader('webpack-glsl-loader')
        .end()
  }
}