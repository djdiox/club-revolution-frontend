# club-revolution-frontend
Welcome to my vision of a modern visualization app based on Vue.JS.

This project is created with vue-cli and hosted in (GitLab)[https://gitlab.com/djdiox/club-revolution-frontend]
There is an automatic build in GitLab CI/CD and hosted in Gitlab Pages.

You can find the frontend here:
(https://djdiox.gitlab.io/club-revolution-frontend/)[https://djdiox.gitlab.io/club-revolution-frontend/]

Vuetify is enabled, the visualization are based on Three.JS and tutorials of the internet.

TODO:     
    https://github.com/Eronne/threejs-audio-visualizer/tree/master/app
    http://raathigesh.com/Audio-Visualization-with-Web-Audio-and-ThreeJS/
    https://libraries.io/github/mattdesl/awesome-audio-visualization
    https://codepen.io/soulwire/full/Dscga/
    https://codepen.io/njmcode/full/WbWyWz/

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your end-to-end tests
```
yarn run test:e2e
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
